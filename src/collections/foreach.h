#pragma once

#define FOR_EACH(PFX, SNAME, TARGET) \
    for (SNAME##_iter iter = TARGET->it_start(TARGET); \
         !PFX##_iter_end(&iter); \
         PFX##_iter_next(&iter))

#define FOR_EACH_REV(PFX, SNAME, TARGET) \
    for (SNAME##_iter iter = T->it_end(T); \
         !PFX##_iter_start(&iter); \
         PFX##_iter_prev(&iter))
