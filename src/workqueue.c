#include "collections/queue.h"
#include "collections/foreach.h"

#include <workqueue.h>
#include <pthread.h>
#include <errno.h>

#include <stdio.h>

/*
 * FIXME Need to have more strict control over
 * the queue size to prevent the queue growing
 * to uncontrollable sizes. In particular, we
 * need a set max (because everything needs a
 * set maximum).
 */
#define INITIAL_QUEUE_SIZE 256

/*
 * Generate a queue with the name of wqq.
 * Functions will be prefixed with wqq_
 * All function implementations are static.
 * The queue will hold instances of struct work.
 * Create a FOR_EACH macro that will iterate through the structure.
 */
QUEUE_GENERATE(wqq, wqq, static, struct work*);
#define WQQ_FOR_EACH(Target) FOR_EACH(wqq, wqq, Target)

struct workqueue
{
	pthread_t *threads;
	int num_threads;

	pthread_mutex_t mutex;
	pthread_cond_t cond;

	pthread_mutex_t idle_mutex;
	pthread_cond_t idle_cond;
	bool idle;

	int active_workers;
	bool quitting;

	wqq *queue;
};

static bool worker_wait(struct workqueue *workqueue)
{
	wqq *queue = workqueue->queue;

	while (wqq_empty(queue)) {
		if (workqueue->quitting)
			return false;

		printf("Sleepy sheepy\n");

		pthread_cond_wait(&workqueue->cond, &workqueue->mutex);

		printf("Wakey wakey (or spurious wakeup)\n");
	}

	return true;
}

static void *worker(void *arg)
{
	struct workqueue *workqueue = arg;
	wqq *queue = workqueue->queue;

	pthread_mutex_lock(&workqueue->mutex);

	/* worker_wait expects a locked mutex.
	 * When it exits, the calling thread
	 * will own the mutex again. */
	for (;;) {
		if (!worker_wait(workqueue)) {
			pthread_mutex_unlock(&workqueue->mutex);
			return NULL;
		}

		/* Have our state reflect that we're not idle */
		workqueue->idle = false;

		/* We have work to do and we own the mutex.
		 * We need to unlock as fast as possible.
		 * So simply pop our work and unlock. */
		struct work *work = wqq_peek(queue);
		wqq_dequeue(queue);

		/* Increase our active worker counter */
		++workqueue->active_workers;

		pthread_mutex_unlock(&workqueue->mutex);
		work->fn(work);

		/* Signal waiters for this specific work that we're done */
		pthread_cond_signal(&work->finished_cond);

		/* Make sure we obtain our lock before
		 * looping. We *cannot* call wait with an
		 * unlocked mutex. Consider it undefined
		 * behavior */
		pthread_mutex_lock(&workqueue->mutex);

		/* We're done with work for now, decrease active workers */
		--workqueue->active_workers;

		/* If no work left and no active workers, signal that we're idle. */
		if (wqq_empty(workqueue->queue) && workqueue->active_workers == 0) {
			printf("Idle Igloo\n");
			workqueue->idle = true;
			pthread_cond_signal(&workqueue->idle_cond);
		}
	}

	return NULL;
}

/* Cancels and joins `num_threads` threads going from top to bottom */
static void destroy_workers(pthread_t *threads, int num_threads)
{
	for (int i = num_threads - 1; i >= 0; --i) {
		pthread_t thread = threads[i];

		pthread_join(thread, NULL);
	}
}

static int spawn_workers(
  pthread_t *threads,
  int num_threads,
  struct workqueue *wq
) {
	int error = 0;

	for (int i = 0; i < num_threads; ++i) {
		pthread_t *thread = &threads[i];
		error = pthread_create(thread, NULL, worker, wq);

		if (error != 0) {
			/* We've failed, revert by destroying
			 * threads we've already made */
			wq->quitting = true;
			destroy_workers(threads, i + 1);
			break;
		}
	}

	return error;
}

struct workqueue *wq_create(int num_threads)
{
	pthread_t *threads = 0;
	wqq *queue = 0;
	struct workqueue *result = 0;
	int error = 0;

	if (num_threads < 1) {
		errno = EINVAL;
		goto fail_bad_argument;
	}

	threads = calloc(num_threads, sizeof(pthread_t));

	if (!threads) {
		errno = ENOMEM;
		goto fail_threads_alloc;
	}

	queue = wqq_new(INITIAL_QUEUE_SIZE);

	if (!queue) {
		errno = ENOMEM;
		goto fail_queue_alloc;
	}

	result = malloc(sizeof(struct workqueue));

	if (!result) {
		errno = ENOMEM;
		goto fail_workqueue_alloc;
	}

	*result = (struct workqueue) {
		.threads = threads,
		.num_threads = num_threads,
		.mutex = PTHREAD_MUTEX_INITIALIZER,
		.cond = PTHREAD_COND_INITIALIZER,
		.idle_mutex = PTHREAD_MUTEX_INITIALIZER,
		.idle_cond = PTHREAD_COND_INITIALIZER,
		.queue = queue,
	};

	error = spawn_workers(threads, num_threads, result);

	if (error != 0) {
		errno = error;
		goto fail_spawn_workers;
	}

	return result;

fail_spawn_workers:
	free(result);

fail_workqueue_alloc:
	wqq_free(queue);

fail_queue_alloc:
	free(threads);

fail_threads_alloc:
fail_bad_argument:
	return NULL;
}

void wq_destroy(struct workqueue *workqueue)
{
	/* FIXME quitting needs to be atomic but
	 * we don't have an atomic API right now. */

	/* Set our quitting state and give each thread some work
	 * so they can check the state */
	workqueue->quitting = true;
	pthread_cond_broadcast(&workqueue->cond);
	destroy_workers(workqueue->threads, workqueue->num_threads);

	pthread_cond_destroy(&workqueue->cond);
	pthread_cond_destroy(&workqueue->idle_cond);
	pthread_mutex_destroy(&workqueue->mutex);
	pthread_mutex_destroy(&workqueue->idle_mutex);
	wqq_free(workqueue->queue);
	free(workqueue->threads);
	free(workqueue);
}

bool wq_queue(struct workqueue *workqueue, struct work *work)
{
	pthread_mutex_lock(&workqueue->mutex);

	wqq_enqueue(workqueue->queue, work);

	pthread_mutex_unlock(&workqueue->mutex);

	pthread_cond_signal(&workqueue->cond);

	return true;
}

int wq_queue_delayed(
  struct workqueue *workqueue,
  struct work *work,
  int delay
) {
	/* TODO */
	return 0;
}

int wq_cancel_delayed_work(struct work *work)
{
	/* TODO */
	return 0;
}

void wq_flush(struct workqueue *workqueue)
{
	pthread_mutex_lock(&workqueue->idle_mutex);

	while (!workqueue->idle) {
		pthread_cond_wait(&workqueue->idle_cond, &workqueue->idle_mutex);
	}

	pthread_mutex_unlock(&workqueue->idle_mutex);
}

bool wq_flush_work(struct work *work)
{
	bool result = false;

	pthread_mutex_lock(&work->finished_mutex);

	while(!work->finished) {
		pthread_cond_wait(&work->finished_cond, &work->finished_mutex);
	}

	pthread_mutex_unlock(&work->finished_mutex);

	return result;
}