#include <pthread.h>
#include <stdbool.h>

#define INIT_WORK(Work, Func)                   \
	(*(Work)) = (struct work) {                  \
		.fn = (Func),                             \
		.finished_cond = PTHREAD_COND_INITIALIZER, \
		.finished_mutex = PTHREAD_MUTEX_INITIALIZER \
	}

#define DECLARE_WORK(Name, Func)                \
	struct work * Name = (struct work) {         \
		.fn = (Func),                             \
		.finished_cond = PTHREAD_COND_INITIALIZER, \
		.finished_mutex = PTHREAD_MUTEX_INITIALIZER \
	}


struct workqueue;
struct work;

typedef void work_fn(struct work *work);

struct work
{
	work_fn *fn;

	bool finished;
	pthread_mutex_t finished_mutex;
	pthread_cond_t finished_cond;
};

/**
 * Create a new workqueue.
 * Note that if it fails to create num_threads,
 * it will fail.
 *
 * @param num_threads Number of threads associated with the workqueue
 * @return workqueue instance with num_threads threads.
 */
struct workqueue *wq_create(int num_threads);

/**
 * Destroy resources associated with workqueue
 *
 * @param workqueue Workqueue to destroy
 */
void wq_destroy(struct workqueue *workqueue);

/**
 * Queue work to asynchronously execute over workqueue
 *
 * @param workqueue The workqueue to execute work
 * @param work The work to execute in workqueue
 * @return true if successful, false if work was already in queue.
 */
bool wq_queue(struct workqueue *workqueue, struct work *work);

/**
 * Waits until all *current* work is finished.
 * If a seperate thread is queueing work, that
 * will potentially create a race condition
 * with this function since it will wait until
 * the work queue is depleted.
 *
 * @param workqueue The workqueue to wait on
 */
void wq_flush(struct workqueue *workqueue);

/**
 * Waits for a particular work to finish
 *
 * @param work Work to wait on
 * @return true if waited, false if already idle
 */
bool wq_flush_work(struct work *work);