#include <workqueue.h>
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

#define AMOUNT_WORK 16
#define DUMB_ITERATIONS INT_MAX
#define NUM_THREADS 1

struct random_work
{
	struct work work;
	int id;
};

void do_work(struct work *data)
{
	struct random_work *work = (struct random_work*)data;

	/* Do some pointless work */
	volatile int dumb = 0;
	for (int i = 0; i < DUMB_ITERATIONS; ++i);

	printf("%d\n", work->id);
}

int main(int argc, char *argv[])
{
	/* Create a workqueue on top of a 4-thread pool */
	struct workqueue *wq = wq_create(NUM_THREADS);
	struct random_work *work = malloc(sizeof *work * AMOUNT_WORK);

	/* Initialize our work structures */
	for (int i = 0; i < AMOUNT_WORK; ++i) {
		INIT_WORK(&work[i].work, do_work);
		work[i].id = i;

		/* Queue our initialized work */
		wq_queue(wq, (struct work*)&work[i]);
	}

	/* Wait for all of our work to finish */
	wq_flush(wq);

	/* Clean up our workqueue */
	wq_destroy(wq);
}